import Vue from 'vue'
import i18n from './plugins/i18n'
import App from './components/App.vue'
import './assets/styles/app.css'

Vue.config.productionTip = false

Vue.use(i18n)

new Vue({
  render: h => h(App),
}).$mount('#app')
