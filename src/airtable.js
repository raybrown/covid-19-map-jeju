import Airtable from 'airtable'

const {
  VUE_APP_AIRTABLE_API_ENDPOINT,
  VUE_APP_AIRTABLE_API_KEY,
  VUE_APP_AIRTABLE_BASE_ID,
} = process.env

const BASE = new Airtable({
  endpointUrl: VUE_APP_AIRTABLE_API_ENDPOINT,
  apiKey: VUE_APP_AIRTABLE_API_KEY,
}).base(VUE_APP_AIRTABLE_BASE_ID)

export default BASE
