const { colors } = require('tailwindcss/defaultTheme')

module.exports = {
  theme: {
    extend: {
      colors: {
        gray: {
          ...colors.gray,
          950: '#0b0e13',
        },

        'mapbox-water': '#a7cbd7',
      },

      maxWidth: {
        '2xs': '15rem',
      },

      minHeight: {
        12: '3rem',
      },

      spacing: {
        em: '1em',
      },

      width: {
        'max-content': 'max-content',
      },
    },
  },
  variants: {
    // backgroundColor: ['responsive', 'hover', 'focus'],
    backgroundColor: ['responsive', 'hover', 'focus', 'active'],
  },
  plugins: [],
}
