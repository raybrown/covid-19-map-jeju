require('dotenv').config()

/* eslint-disable no-console */

// import dependencies
const fs = require('fs')
const Airtable = require('airtable')
const geoJSON = require('geojson')

// store env vars
const apiKey = process.env.VUE_APP_AIRTABLE_API_KEY
const baseId = process.env.VUE_APP_AIRTABLE_BASE_ID
const endpointUrl = process.env.VUE_APP_AIRTABLE_API_ENDPOINT

// Set up the Airtable connection
const DB = new Airtable({ endpointUrl, apiKey }).base(baseId)

// Utility method to save files
const saveFile = (tableName, fileName, data) => {
  const file = `./public/db/${fileName}`

  fs.writeFile(file, JSON.stringify(data), (err) => {
    if (err) console.log(err)
    else console.log(`'${tableName}' records saved to '${file}'.`)
  })
}

// Method to save 'text' tables records to JSON
const getI18n = () => {
  const allRecords = []

  DB('text').select({ fields: ['en', 'ko', 'slug'], maxRecords: 500 }).eachPage(
    // Success
    (records, fetchNextPage) => {
      Array.prototype.push.apply(allRecords, records.map(r => r._rawJson))
      fetchNextPage()
    },

    // Done
    (err) => {
      if (err) return err
      saveFile('text', 'i18n.json', allRecords)
    },
  )
}

// Method to save 'places' and 'routes' as geoJSON
const getGeoJSON = () => {
  const params = {
    cases: {
      fields: ['activities'],
      filterByFormula: '{isPublished}',
    },

    places: {
      fields: ['activities', 'coordinates', 'mapServiceAddress'],
    },

    routes: {
      fields: ['activities', 'coordinates'],
    },
  }

  // First, get all published cases
  DB('cases').select(params.cases).firstPage((err, casesRecords) => {
    if (err) { console.error(err); return }

    // Store an array of activities related to the published cases
    const activityIDs = []
    casesRecords.forEach(r => {
      Array.prototype.push.apply(activityIDs, r.fields.activities)
    })

    // Then, get all place records
    DB('places').select(params.places).firstPage((err, placeRecords) => {
      if (err) { console.error(err); return }

      // ...but only add them to the `places` array if they're linked to
      // one of the activities in the array we previously made
      const places = []

      placeRecords.forEach(r => {
        const include = r.fields.activities.some(a => activityIDs.includes(a)) && r.fields.coordinates
        if (!include) return

        places.push({
          coordinates: JSON.parse(r.fields.coordinates),
          id: r.id,
          mapServiceAddress: r.fields.mapServiceAddress ? r.fields.mapServiceAddress[0] : null,
        })
      })

      // Then, get all route records
      DB('routes').select(params.routes).firstPage((err, routeRecords) => {
        if (err) { console.error(err); return }

        // Make the same check that we did for the places.
        const routes = []

        routeRecords.forEach(r => {
          const include = r.fields.activities.some(a => activityIDs.includes(a)) && r.fields.coordinates
          if (!include) return

          routes.push({
            coordinates: JSON.parse(r.fields.coordinates),
            id: r.id,
          })
        })

        // Parse places and routes as geoJSON
        const placesGeoJSON = geoJSON.parse(places, {
          include: ['id', 'mapServiceAddress'],
          Point: 'coordinates',
        })

        const routesGeoJSON = geoJSON.parse(routes, {
          include: ['id'],
          LineString: 'coordinates',
        })

        // Finaly, save the geoJSON to file
        saveFile('places', 'places.geojson', placesGeoJSON)
        saveFile('routes', 'routes.geojson', routesGeoJSON)
      })
    })
  })
}

// Run the methods
getGeoJSON()
getI18n()
