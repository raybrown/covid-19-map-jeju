import records from '../../public/db/i18n.json'
import EventBus from '@/event-bus'

const MultipleLocales = {
  install (Vue) {
    Vue.prototype.$i18n = {
      get: (key = null, keyType = 'id') => {
        const { locale, records } = Vue.prototype.$i18n
        let record = null

        if (keyType === 'id') {
          record = records.find(r => r[keyType] === key)
        } else {
          record = records.find(r => r.fields[keyType] === key)
        }

        if (!record) {
          console.error(`Couldn't find i18n record with ${keyType}: ${key}`)
          return
        }

        return record.fields[locale]
      },
      locale: process.env.VUE_APP_LOCALE || 'en',
      records: records,
    }

    EventBus.$on('i18n:switch', () => {
      const targetLocale = Vue.prototype.$i18n.locale === 'en' ? 'ko' : 'en'

      document.documentElement.lang = targetLocale
      Vue.prototype.$i18n.locale = targetLocale
      EventBus.$emit('i18n:switched', targetLocale)
    })
  },
}

export default MultipleLocales
