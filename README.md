# covid-19-map-jeju

This project displays information about the COVID-19 outbreak as it pertains to Jeju Island, South Korea. The island has relatively low outbreak numbers and a responsive government who shares data freely with island residents. The sharing of that data inspired me to build this project; I was aware that COVID-19 cases were on the island, but I was having trouble understanding *where* the cases were. My hope is that the project helps myself and others better visualize this information.

## Setup the project

> Before you begin, you should know that this project relies heavily on [Airtable](https://airtable.com/) and [Mapbox](https://mapbox.com). You will need to create accounts and API keys for both of these services before you'll be able to run this project locally. If you're okay with that, carry on! If not, thanks for your time up to this point.

### Basic Installation

1. Run `npm install`
2. Duplicate `.env.example` as `.env`
3. Configure Airtable
4. Configure Mapbox

#### Airtable Setup

3a. Visit the ['COVID-19 Map: Jeju' Airtable](https://airtable.com/shrsJTdNo2GGNkNoW) and click on the "Copy Base" button in the top right corner. This will walk you through the process of creating a free Airtable account and cloning the project database into your own environment.

3b. Once you're all set up with Airtable, head to [your Airtable account](https://airtable.com/account) and configure the API key settings there. Back on your machine, open `.env` and add your API key to the `VUE_APP_AIRTABLE_API_KEY` env variable.

3c. Then, head back to your new Airtable base. Click on the "Help" link in the top right corner, and then click on "API Documentation". You should see the ID of your base displayed on that page. Add it to the `VUE_APP_AIRTABLE_BASE_ID` env variable.

3d. Finally, add `https://api.airtable.com` as the `VUE_APP_AIRTABLE_API_ENDPOINT` env variable.

#### Mapbox Setup

4a. Head over to [Mapbox](https://mapbox.com) and create a free account. Once you're done with that, go to [your account page](https://account.mapbox.com/) and create an access token.

4b. Back on your machine, open `.env` and add the access token to the `VUE_APP_MAPBOX_ACCESS_TOKEN` env variable. While you're there, add `mapbox://styles/bitmanic/ck88l7jxb28tz1iqiz1v5ej69` as the `VUE_APP_MAPBOX_STYLE_URL` env variable.

Once you've completed all of the above steps, you're finally ready to develop!

## Compile and hot-reload for development

Run `npm run serve` to pull the data down from Airtable and start a local development server. You'll be able to access the site at [http://localhost:8080/](http://localhost:8080/). The server will recompile as you save files.

## Compile and minifies for production
```
npm run build
```

## Lints and fixes files
```
npm run lint
```

## Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
